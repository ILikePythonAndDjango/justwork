## justwork

## Usage

# Stack

Python==3.6.6

PostgreSQL==9.5.10

Django==2.0.5

djangorestframework==3.8.2

pytz==2018.5

# How to run?

`docker-compose up runserver`

# How to testing?

`docker-compose up autotests`

# How to create admin user

`docker-compose up runserver python3 content/manage.py createsuperuser`

# API tutorial

/pages/?page=1&limit=2 GET POST

#App api have pagination such as `/pages/`.

`page=1` and `limit=5` by default

`page` and `limit` are query params for pagination.

```
[
    {
        "id": 1,
        "url": "http://localhost:8000/pages/43/",
        "title": "Rename",
        "video_set": [
            4
        ],
        "audio_set": [],
        "text_set": []
    },
    {
        "id": 2,
        "url": "http://localhost:8000/pages/2/",
        "title": "JustWork",
        "video_set": [],
        "audio_set": [],
        "text_set": []
    }
]
```

If `content_with_detail` contains any value than server returns JSON with more detail
info about contents(`/pages/?page=1&limit=2&conten_with_detail=1`).

```
[
    {
        "id": 1,
        "url": "http://localhost:8000/pages/43/",
        "title": "Rename",
        "video_set": [
            {
                "id": 4,
                "url": "http://localhost:8000/videos/4/",
                "title": "Quaerat in expedita nulla animi totam accusantium, velit sit consequatur nemo",
                "page": 43,
                "content": null,
                "counter": 22
            }
        ],
        "audio_set": [],
        "text_set": []
    },
    {
        "id": 2,
        "url": "http://localhost:8000/pages/2/",
        "title": "JustWork",
        "video_set": [],
        "audio_set": [],
        "text_set": []
    }
]
```

/pages/48/ GET PUT DELETE

```
{
    "id": 48,
    "url": "http://localhost:8000/pages/48/",
    "title": "dsrew",
    "video_set": [],
    "audio_set": [
        2
    ],
    "text_set": [
        1
    ]
}
```

If `content_with_detail` contains any value than server returns JSON with more detail
info about contents(`/pages/48/?page=1&limit=2&conten_with_detail=1`).

```
{
    "id": 48,
    "url": "http://localhost:8000/pages/48/",
    "title": "dsrew",
    "video_set": [],
    "audio_set": [
        {
            "id": 2,
            "url": "http://localhost:8000/audios/2/",
            "title": "Pantera",
            "page": 48,
            "content": "http://localhost:8000/pages/48/content/audio/Pantera_-_Walk_Official_Video.mp3",
            "counter": 23
        }
    ],
    "text_set": [
        {
            "id": 1,
            "url": "http://localhost:8000/texts/1/",
            "title": "Log",
            "page": 48,
            "content": "http://localhost:8000/pages/48/content/text/InstallationLog.txt",
            "counter": 25
        }
    ]
}
```

#If you use request with `/pages/?content_with_detail=ds` or `/pages/1/?content_with_detail=er` 
then `counter` increase for all content type

/texts/?page=1&limit=2 GET POST

```
[
    {
        "id": 2,
        "url": "http://localhost:8000/texts/2/",
        "title": "python_byte_code_example",
        "page": 50,
        "content": "http://localhost:8000/texts/content/text/tests.cpython-35.pyc",
        "counter": 21
    },
    {
        "id": 1,
        "url": "http://localhost:8000/texts/1/",
        "title": "Log",
        "page": 48,
        "content": "http://localhost:8000/texts/content/text/InstallationLog.txt",
        "counter": 25
    }
]
```

/texts/1/ GET PUT DELETE

```

{
    "id": 1,
    "url": "http://localhost:8000/texts/1/",
    "title": "Log",
    "page": 48,
    "content": "http://localhost:8000/texts/1/content/text/InstallationLog.txt",
    "counter": 25
}
```

/videos/?page=1&limit=2 GET POST

```
[
    {
        "id": 4,
        "url": "http://localhost:8000/videos/4/",
        "title": "Quaerat in expedita nulla animi totam accusantium, velit sit consequatur nemo",
        "page": 43,
        "content": null,
        "counter": 23
    },
    {
        "id": 1,
        "url": "http://localhost:8000/videos/1/",
        "title": "Python",
        "page": 49,
        "content": "http://localhost:8000/videos/content/video/%D0%9E%D1%82%D0%B7%D1%8B%D0%B2_%D0%BE_LearnPython_%D0%BE%D1%82_%D0%BF%D1%80%D0%BE%D0%B3%D1%80%D0%B0%D0%BC%D0%BC%D0%B8%D1%81%D1%82%D0%B0_%D1%81_%D0%BE%D0%BF%D1%8B%D1%82%D0%BE%D0%BC1.mp4",
        "counter": 21
    }
```

/videos/1/ GET PUT DELETE

```
{
    "id": 1,
    "url": "http://localhost:8000/videos/1/",
    "title": "Python",
    "page": 49,
    "content": "http://localhost:8000/videos/1/content/video/%D0%9E%D1%82%D0%B7%D1%8B%D0%B2_%D0%BE_LearnPython_%D0%BE%D1%82_%D0%BF%D1%80%D0%BE%D0%B3%D1%80%D0%B0%D0%BC%D0%BC%D0%B8%D1%81%D1%82%D0%B0_%D1%81_%D0%BE%D0%BF%D1%8B%D1%82%D0%BE%D0%BC1.mp4",
    "counter": 21
}
```

/audios/ GET POST

```
[
    {
        "id": 1,
        "url": "http://localhost:8000/audios/1/",
        "title": "Kiss",
        "page": 50,
        "content": "http://localhost:8000/audios/content/audio/Kiss_-_I_Was_Made_For_Lovin_You_Version_Original_1979_Producciones_Especiales_Jose__DJ_Mix.mp3",
        "counter": 21
    },
    {
        "id": 2,
        "url": "http://localhost:8000/audios/2/",
        "title": "Pantera",
        "page": 48,
        "content": "http://localhost:8000/audios/content/audio/Pantera_-_Walk_Official_Video.mp3",
        "counter": 23
    }
]
```

/audios/1/ GET PUT DELETE

```
{
    "id": 1,
    "url": "http://localhost:8000/audios/1/",
    "title": "Kiss",
    "page": 50,
    "content": "http://localhost:8000/audios/1/content/audio/Kiss_-_I_Was_Made_For_Lovin_You_Version_Original_1979_Producciones_Especiales_Jose__DJ_Mix.mp3",
    "counter": 21
}
```

# Why didn't I use Celery?

I think this task(increase field) isn't difficult. I'd been using `django.db.models.F` for it. 