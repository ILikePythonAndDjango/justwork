from django.urls import reverse
from django.core.files.uploadedfile import SimpleUploadedFile, InMemoryUploadedFile
from django.core.files.storage import Storage
from rest_framework import status
from rest_framework.test import APITestCase
from .models import *
import json
import os

dir = os.path.dirname(os.path.abspath(__file__))

class PageTest(APITestCase):

    def setUp(self):
        self.page = Page.objects.create(
            title = 'page'
        )

    def test_create_page(self):
        data = {
            "id": 2,
            "title": "myprettyhomepage",
            'video_set': [],
            'text_set': [],
            'audio_set': [],
        }

        response = self.client.post('/pages/', data, format='json')

        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(Page.objects.exists(), True)
        self.assertEqual(Page.objects.get(pk=2).title, 'myprettyhomepage')

    def test_page_list(self):
        response = self.client.get('/pages/', format='json')

        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_page_detail(self):
        response = self.client.get('/pages/{}/'.format(self.page.id), format='json')

        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_update_page(self):
        data = {'title': 'rename'}

        response = self.client.put('/pages/{}/'.format(self.page.id), data, format='multipart')

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(Page.objects.get(pk=self.page.id).title, 'rename')

    def test_delete_page(self):
        response = self.client.get('/pages/{}/'.format(self.page.id), format='json')

        self.assertEqual(response.status_code, status.HTTP_200_OK)


class TextTestCase(APITestCase):

    def setUp(self):

        #creating abstract text file for 
        self.file = SimpleUploadedFile('text.txt', b'Lorem ipsum dolor sit amet, consectetuer adipiscing elit.')

        # create Page for text
        self.page = Page.objects.create(
            title='test'
        )

        # create some Text
        self.text = Text.objects.create(
            title='test',
            page=Page.objects.get(pk=self.page.id),
            content=self.file
        )

    def test_create_text(self):

        with open(dir + '/test_files/test_text.txt', 'rb') as text_file:
            data = {
                'page': self.page.id,
                'title': 'second_test',
                'content': text_file,
            }

            response = self.client.post('/texts/', data, format='multipart')

        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

    def test_text_list(self):
        response = self.client.get('/texts/', format='json')

        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_text_detail(self):
        response = self.client.get('/texts/{}/'.format(self.text.id), format='json')

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(self.text.title, response.data['title'])

    '''def test_update_text(self):
                    data = {
                        "title": "Log",
                    }
            
                    response = self.client.put('/texts/{}/'.format(self.text.id), data, content_type='multipart/form-data')
            
                    self.assertEqual(response.status_code, status.HTTP_200_OK)
                    self.assertEqual(Text.objects.get(id=self.text.id).title, data['title'])'''


    def test_delete_text(self):
        response = self.client.delete('/texts/{}/'.format(self.text.id), format='json')

        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)

class VideoTestCase(APITestCase):

    def setUp(self):

        #creating abstract text file for 
        self.file = SimpleUploadedFile('text_video.txt', b'Lorem ipsum dolor sit amet, consectetuer adipiscing elit.')

        # create Page for text
        self.page = Page.objects.create(
            title='test_video'
        )

        # create some Text
        self.video = Text.objects.create(
            title='test',
            page=Page.objects.get(pk=self.page.id),
            content=self.file
        )

    def test_create_video(self):

        with open(dir + '/test_files/test_video.txt', 'rb') as text_file:
            data = {
                'page': self.page.id,
                'title': 'second_test',
                'content': text_file,
            }

            response = self.client.post('/videos/', data, format='multipart')

        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

    def test_video_list(self):
        response = self.client.get('/videos/', format='json')

        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_video_detail(self):
        response = self.client.get('/videos/{}/'.format(self.video.id), format='json')

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(self.video.title, response.data['title'])

    def test_delete_video(self):
        response = self.client.delete('/videos/{}/'.format(self.video.id), format='json')

        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)