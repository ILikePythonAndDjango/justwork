from django.db import models

__all__ = ('Page', 'Video', 'Audio', 'Text')

# If you append new model then you must append name into __all__

def user_directory_path(instance, filename):
    # file will be uploaded to MEDIA_ROOT/<directory_name>/<filename>
    return '{0}/{1}'.format(instance.directory_name, filename)

class Page(models.Model):

    title = models.CharField(max_length=100)

    def __str__(self):
        return self.title

class Content(models.Model):

    title = models.CharField(max_length=100, db_index=True)
    page = models.ForeignKey('Page', on_delete=models.CASCADE)
    content = models.FileField(upload_to=user_directory_path, unique=True)
    counter = models.IntegerField(default=0)

    def __str__(self):
        return self.title

    class Meta:
        abstract = True

class Video(Content):

    directory_name = 'video'

class Audio(Content):

    directory_name = 'audio'

class Text(Content):

    directory_name = 'text'