from django.urls import path, include

from rest_framework import routers

from . import views

router = routers.DefaultRouter()
router.register(r'pages', views.PageViewSet)
router.register(r'texts', views.TextViewSet)
router.register(r'audios', views.AudioViewSet)
router.register(r'videos', views.VideoViewSet)

urlpatterns = (
    path('', include(router.urls)),
)