from django.shortcuts import render
from django.http import HttpResponse
from django.db.models import F

from rest_framework.viewsets import ModelViewSet
from rest_framework.response import Response
from rest_framework.request import Request

from .serializers import *
from .models import *

import json

httpmns = ['get', 'post', 'put', 'delete', 'head', 'options']

def update_counter_page(page):
    '''
    increase counter when server have request
    '''
    page.video_set.annotate().update(counter=F('counter') + 1)
    page.text_set.annotate().update(counter=F('counter') + 1)
    page.audio_set.annotate().update(counter=F('counter') + 1)

def update_counter_list_page(querysetlist):
    for page in querysetlist:
        update_counter_page(page)

def pagination(view):

    '''
    This is decorator that paginate own data
    `page` is number current page
    `limit` is number objects on some page
    '''

    def get_valid_query_params(self, request, *args, **kwargs):
        try:
            limit = abs(int(request.query_params.get('limit', 5)))
            page = abs(int(request.query_params.get('page', 1)))
            page = 1 if page == 0 else page
        except ValueError:
            return Response({"Error message": "invalid get params"})

        self.queryset = self.queryset[(page-1)*limit:page*limit]
        return view(self, request, *args, **kwargs)

    return get_valid_query_params

def content_with_detail(view):

    '''
    If you use `content_with_detail=1` as get param then you get content set more detailed than without it.
    '''

    def get_request(self, request, *args, **kwargs):
        if request.query_params.get('content_with_detail', 0):
            self.serializer_class = PageWithDetailSerializer
        return view(self, request, *args, **kwargs)

    return get_request

class PaginationMixin(ModelViewSet):

    @pagination
    def list(self, request, *args, **kwargs):
        return super(PaginationMixin, self).list(request, *args, **kwargs)


class PageViewSet(PaginationMixin):

    http_method_names = httpmns

    serializer_class = PageSerializer
    queryset = Page.objects.all()

    @content_with_detail
    def list(self, request, *args, **kwargs):
        if request.query_params.get('content_with_detail', 0):
            update_counter_list_page(self.queryset)
        return super(PageViewSet, self).list(request, *args, **kwargs)

    @content_with_detail
    def retrieve(self, request, pk=None):
        if request.query_params.get('content_with_detail', 0):
            update_counter_page(self.queryset.get(pk=pk))
        return super(PageViewSet, self).retrieve(request, pk=None)

class TextViewSet(PaginationMixin):

    http_method_names = httpmns

    serializer_class = TextSerializer
    queryset = Text.objects.all()

class AudioViewSet(PaginationMixin):

    http_method_names = httpmns

    serializer_class = AudioSerializer
    queryset = Audio.objects.all()

class VideoViewSet(PaginationMixin):

    http_method_names = httpmns

    serializer_class = VideoSerializer
    queryset = Video.objects.all()