from rest_framework.serializers import ModelSerializer

from django.db.models import F

from .models import *

# mainly fields
fields_for_page = ('id', 'url', 'title', 'video_set', 'audio_set', 'text_set')
fields_for_content = ('id', 'url', 'title', 'page', 'content', 'counter')


class PageSerializer(ModelSerializer):

    class Meta:
        model = Page
        fields = fields_for_page


class TextSerializer(ModelSerializer):

    class Meta:
        model = Text
        fields = fields_for_content


class AudioSerializer(ModelSerializer):

    class Meta:
        model = Audio
        fields = fields_for_content


class VideoSerializer(ModelSerializer):

    class Meta:
        model = Video
        fields = fields_for_content


class PageWithDetailSerializer(PageSerializer):

    video_set = VideoSerializer(many=True)
    audio_set = AudioSerializer(many=True)
    text_set = TextSerializer(many=True)
