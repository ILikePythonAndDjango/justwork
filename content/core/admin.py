from django.contrib import admin
from .models import *

class ContentAdmin(admin.ModelAdmin):

    search_fields = ('^title',)

admin.site.register(Page, ContentAdmin)
admin.site.register(Video, ContentAdmin)
admin.site.register(Audio, ContentAdmin)
admin.site.register(Text, ContentAdmin)